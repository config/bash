#!/bin/bash
# ~/.bashrc: executed by bash(1) for non-login shells.

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# system-wide bash completion
[ -f /etc/bash_completion ] && . /etc/bash_completion

# user-defined aliases
aliases_dir="$HOME/.bash/aliases.d"
if [[ -d $aliases_dir && -r $aliases_dir && -x $aliases_dir ]]; then
  for i in "$aliases_dir"/*; do
    [[ -f $i && -r $i ]] && . "$i"
  done
fi
unset aliases_dir i

# env variables
[ -f ~/.bash/env ] && . ~/.bash/env

# LOCAL env variables, these settings are not meant to be shared
[ -f ~/.bash/env_local ] && . ~/.bash/env_local

# load git prompt support
[ -f /usr/lib/git-core/git-sh-prompt ] && . /usr/lib/git-core/git-sh-prompt

# load function to set custom shell prompts
if [ -f ~/.bash/bash_prompt/set_prompt ];
then
  . ~/.bash/bash_prompt/set_prompt

  # set a different prompt for the root user
  if [ $UID -eq 0 ];
  then
    set_prompt fire
  else
    set_prompt iceg
  fi 
fi
